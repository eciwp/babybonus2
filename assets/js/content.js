$(document).ready(function() {
  "use strict";
  initSlide();
  initHeaderMenu();
  initPopOver();
  initChangeSize();
  initDropdownSubmenu();
  initLifeStageLink();
  initCallOut();
  $('.print-page').bind('click', function() {
    window.print();
  });
});
$(window).resize(function() {
  "use strict";
});
/* HEADER MENU */
function initHeaderMenu() {
  "use strict";
  $('.menu-mobile-btn').bind('click', function() {
    $(this).find('a').toggleClass('nav-is-visible');
    $('.nav.navbar-nav.navbar-left.main-menu').slideToggle();
  });
  $(window).resize(function() {
    if ($(window).width() > 800) {
      $('.menu-mobile-btn a').removeClass('nav-is-visible');
      $('.nav.navbar-nav.navbar-left.main-menu').removeAttr('style');
    }
  });
}
/* CHANGE TEXT SIZE */
function initChangeSize() {
  "use strict";
  $('.smaller-text').bind('click', function() {
    $('.resize-text h1, .resize-text h2, .resize-text h3, .resize-text h4, .resize-text h5, .resize-text h6, .resize-text p, .resize-text li a, .resize-text li').each(function() {
      var getSize = $(this).css('font-size');
      $(this).animate({
        "font-size": parseInt(getSize) - 1
      });
    });
  });
  $('.bigger-text').bind('click', function() {
    $('.resize-text h1, .resize-text h2, .resize-text h3, .resize-text h4, .resize-text h5, .resize-text h6, .resize-text p, .resize-text li a, .resize-text li').each(function() {
      var getSize = $(this).css('font-size');
      $(this).animate({
        "font-size": parseInt(getSize) + 1
      });
    });
  });
}
/* SLIDER */
function initSlide() {
  "use strict";
  if (!$('.slider-for').length) {
    return;
  }
  $('.slider-for').on('init', function() {
    $(this).closest('.hero').css('opacity', '1');
  });
  $('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    asNavFor: '.slider-nav'
  });
  $('.slider-nav').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: true,
    arrows: false,
    centerMode: false,
    focusOnSelect: true,
    autoplay: false,
  });
}
/* POP OVER */
function initPopOver() {
  "use strict";
  $('.callout-link').bind('click', function(event) {
    event.preventDefault();
    $('.callout').toggle();
  });
}
/* Dropdown Submenu */
function initDropdownSubmenu() {
  "use strict";
  var $dropdown = $('.dropdown'),
    $dropdown__replacement = $dropdown.find('.dropdown-toggle > .replacement'),
    $dropdown__html = $dropdown__replacement.html();
  $dropdown.find('.dropdown-submenu > a').bind('click', function(e) {
    $(this).parent().removeClass('opened');
    if (!$(this).next('ul').is(':visible')) {
      $(this).parent().addClass('opened');
      $(this).closest('ul').removeClass('opened');
      if ($(this).closest('ul').hasClass('droproot')) {
        $(this).closest('ul').addClass('opened');
        var $html = $(this).html();
        $dropdown__replacement.html($html);
        $(this).closest('.dropdown > .dropdown-toggle > .txt').html($html);
      }
    }
    $(this).next('ul').slideToggle(350);
    e.stopPropagation();
    e.preventDefault();
  });
  var $onthis = $dropdown.find('.dropdown-submenu > a.onthis');
  if ($onthis.length) {
    $onthis.parent().removeClass('opened');
    if (!$onthis.next('ul').is(':visible')) {
      $onthis.parent().addClass('opened');
      $onthis.closest('ul').removeClass('opened');
      if ($onthis.closest('ul').hasClass('droproot')) {
        $onthis.closest('ul').addClass('opened');
        var $html = $onthis.html();
        $dropdown__replacement.html($html);
        $onthis.closest('.dropdown > .dropdown-toggle > .txt').html($html);
      }
    }
    $onthis.next('ul').slideToggle(350);
  }
  $('.dropdown-back').bind('click', function(e) {
    $dropdown__replacement.html($dropdown__html);
    $dropdown.find('.droproot').removeClass('opened');
    $(this).closest('ul').slideToggle(350);
    e.stopPropagation();
    e.preventDefault();
  });
}
/* LifeStage Topics */
function initLifeStageLink() {
  "use strict";
  if ($(window).width() < 768) {
    $('.links-title').bind('click', function(e) {
      $(this).removeClass('opened');
      if (!$(this).next('ul').is(':visible')) {
        $(this).addClass('opened');
      }
      $(this).next('ul').slideToggle(350);
      e.stopPropagation();
      e.preventDefault();
    });
  }
}
/* CALL OUT */
function initCallOut() {
  "use strict";
  $('ul.sidebar-wrapper-links>li>a.has-sub-menu').bind('click', function(event) {
    event.preventDefault();
    $(this).parent('li').toggleClass('active');
    $(this).parent('li').find('.sidebar-wrapper-submenu-links>li').removeClass('active');
  });
  $('ul.sidebar-wrapper-submenu-links a.has-subsubmenu-menu').bind('click', function(event) {
    event.preventDefault();
    $(this).closest('li').toggleClass('active');
  });
}