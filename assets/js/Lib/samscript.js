$(function() {
    var $_dropdown = $('.dropdown-custom');
    $_dropdown.each(function() {
        var $_toggle = $(this).find('ul > li.active > a').text();
        if ($_toggle === '') { $_toggle = 'Select'; } $(this).find('.dropdown-toggle').text($_toggle);
        $(this).find('ul > li > a').on('click', function(e) {
            var a_text = $(this).text();
            $(this).closest('.dropdown-custom').find('.dropdown-toggle').text(a_text);
        });
    });
    $('.carousel-package.sync').each(function(e) {
        $(this).find('.carousel').on('slide.bs.carousel', function(ev) {
            var dir = ev.direction == 'right' ? 'prev' : 'next';
            $('.carousel').not('.sliding').addClass('sliding').carousel(dir);
        });
        $(this).find('.carousel').on('slid.bs.carousel', function(ev) { $('.carousel').removeClass('sliding'); });
    });
    $('footer').on('click', '.footer-chatbox>a', function(e) { try { if (jivo_config.chat_mode) jivo_api.open(); } catch (err) {} });
    $('footer').on('click', '#scrolltop.shown', function(e) {
        var body = $("html, body");
        body.stop().animate({ scrollTop: 0 }, 500, 'swing', function() {});
    });
    $(window).scroll(function() {
        var $body = $('body').height(),
            $window = $(window),
            $offset = $window.height() * 1.5,
            $position = 0;
        var $scroll = $window.scrollTop();
        if ($body > $offset) { $position = $body - $offset; } $('#scrolltop').removeClass('shown');
        if ($scroll >= $position) { $('#scrolltop').addClass('shown'); }
    });
});