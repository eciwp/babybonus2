<!DOCTYPE html>
<html class="no-js" lang="en" xmlns:fb="http://ogp.me/ns/fb#">

<head>
    <meta charset="utf-8" />
    <title>Baby Bonus - TEMPLATE 01</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="wishbone.com">
    <!--#include file="Includes/_styles.htm" -->
    <!--[if lt IE 9]>
      <script type="text/javascript" src="assets/js/Lib/html5shiv-printshiv.js"></script>
      <![endif]-->
</head>

<body class="parenting-resources-page">
    <a class="skip-link skip-access" alt="Skip to content" href="#mainContent">Skip to content</a>
    <!-- Header & Menu -->
    <!--#include file="Includes/menu-pregnancy.shtml" -->
    <!--// Header & Menu -->
    <a id="mainContent" alt="mainContent"></a>
    <!-- Body content -->
    <div class="body-wrap content">
        <div class="container-fluid">
            <!-- Breadcrumb -->
            <ul class="breadcrumb resize-text">
                <li><a href="index.shtml" alt="Home Page">Home</a></li>
                <li><a href="page-parenting-resources.shtml" alt="Parenting Resources">Parenting Resources</a></li>
                <li><a href="page-lifestage-pregnancy.shtml" alt="Pregnancy">Pregnancy</a></li>
                <li><a href="page-lifestage-pregnancy.shtml" alt="Development">Development</a></li>
                <li class="active">Preparing for a baby</li>
            </ul>
            <!--// Breadcrumb -->
            <!-- Template 01 - Content -->
            <div class="row resize-text">
                <div class="col-md-3 col-sm-4 wrapper-sidebar">
                    <!--#include file="Includes/submenu-template02.shtml" -->
                </div>
                <div class="col-md-9 col-sm-8 wrapper-content-template">
                    <div class="wrapper-article-template01">
                        <!-- Template 01 - Content Text -->
                        <div class="content-template-copy">
                            <h2>New baby: preparing children</h2>
                            <p class="pre">When you’re preparing for a baby, your other children might be excited – but maybe a little anxious too. Here are some simple things you can do to help everyone get ready for your new family member. </p>
                            <h5>Preparing for a baby: how children feel</h5>
                            <p>Your other children will have to learn to share your love and attention with the new baby. This can be a big step, especially if your children are still toddlers. They might feel they’re being pushed out of the spotlight.</p>
                            <p>Almost all children need to adjust when a new baby joins the family. But a positive sibling relationship will eventually develop – usually by the time the new baby has reached about 14 months.
                                <br/>
                            </p>
                            <h5>When to tell children about the new baby</h5>
                            <p>If you’re preparing for a baby, when and how much you tell your children about the new baby depends on you. Your children’s ages will also play a part.</p>
                            <p>It can be good to introduce the idea of a new baby fairly early in the pregnancy, perhaps at least three or four months before the baby is born. You could try talking about babies in general and then talk about your new baby.</p>
                            <p>Toddlers don’t really understand time, so when you’re explaining to your child that a new baby is coming, try relating it to a familiar event. You could say that the new baby will arrive soon after a special person’s birthday.</p>
                            <h5>Preparing your other children for the new baby: tips</h5>
                            <p>Before your new baby is born, you can help your other children feel positive about the new member of the family. They need preparation, communication and lots of understanding.</p>
                            <p><strong>Before the birth</strong>
                                <br/> If you can make this a positive and exciting time, your child is more likely to feel that the change is about everybody in the family, not just the new baby. Here are some ideas to help you do this:
                            </p>
                            <ul>
                                <li>Read your children stories about babies. Look at pictures and talk about how your family is growing.</li>
                                <li>Show them pictures of themselves when they were very young.</li>
                                <li>Let them touch mum’s tummy to feel the new baby moving and kicking inside. You could even let them listen to the baby’s heartbeat at a visit to the <a href="#" alt="doctor or midwife">doctor or midwife</a>.</li>
                                <li>Involve them in the practical business of getting ready for the new baby. Let them help you get your home ready, buy baby items and decorate.</li>
                            </ul>
                            <p>It’s also a good idea to give your child <strong>an idea of what it’s like to have a new baby</strong> in the family: </p>
                            <ul>
                                <li>If possible, spend some time with friends and their newborns. Your children will see that new babies are very sleepy and need a lot of care – they’re not instant playmates!</li>
                                <li>Let your children know that the baby will be a separate little person, with its own needs.</li>
                                <li>Encourage your child to socialise and play with other children. This helps your child develop the social skills to have a good relationship with a new sibling. Perhaps you can join a playgroup, or arrange for extended family members – both children and grown-ups – to spend some time with your child.</li>
                            </ul>
                            <h5><em>Older children might want details about where the baby came from, how it got in mum’s tummy, and how it will get out. There are many books about pregnancy and birth. Look for one written for your child’s age and stage. Simple explanations are best.</em></h5>
                            <br/>
                            <h5>During your hospital stay</h5>
                            <p>Here are some ideas to help your children feel OK if mum will be away for a hospital birth:</p>
                            <ul>
                                <li>Let your children know who’ll be looking after them during mum’s hospital stay. If they don’t know the caregiver well, they’ll need time to get used to the idea. </li>
                                <li>Print out some family photos for your other children to keep near their beds while mum is away.</li>
                                <li>Arrange for children’s routines to stay the same as much as possible. This will help them feel more secure.</li>
                                <li>Keep in touch with other children during the hospital stay. If mum and baby are well, make times for the other children to visit.</li>
                            </ul>
                            <h5>When baby comes home</h5>
                            <p>These ideas can help you manage things when your new baby first comes home:</p>
                            <ul>
                                <li>Reassure children (especially toddlers) with a big loving hug before introducing the new baby for the first time.</li>
                                <li>If family and friends are bringing gifts for the new baby, suggest they also bring something small for your other children. You could also have a small gift handy – make it from the new baby to your older children. Young children might like to receive a small doll so they have a ‘baby’ of their own.</li>
                            </ul>
                        </div>
                        <!--// Template 01 - Content Text -->
                        <!-- Template 01 - Content Image -->
                        <div class="wrapper-content-template-image">
                            <div class="content-template-image" style="background-image: url(assets/images/dummy/article/template02/img1.jpg)"></div>
                            <div class="content-template-image-desc">
                                <h3>DID YOU KNOW?</h3>
                                <p>If your child is ready to be toilet trained or to move into a big bed, it’s a good idea to start making these changes well before the baby is born, or to leave these changes until afterwards. </p>
                            </div>
                        </div>
                        <!--// Template 01 - Content Image -->
                    </div>
                </div>
            </div>
            <!--// Template 02 - Content -->
        </div>
    </div>
    <!--// End Body content -->
    <!-- Footer content -->
    <!--#include file="Includes/footer.htm" -->
    <!--// Footer content -->
    <!--#include file="Includes/_scripts.htm" -->
</body>

</html>